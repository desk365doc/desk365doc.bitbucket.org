var searchData=
[
  ['readregconfig',['ReadRegConfig',['../classelex_1_1libupdate_1_1_update_info.html#adde22557b2af1bde2ed6663cea3a362c',1,'elex::libupdate::UpdateInfo']]],
  ['reg_5fclear',['reg_clear',['../classelex_1_1libupdate_1_1_update_config.html#a41a856de915fe2dea7464da79b05ef05',1,'elex::libupdate::UpdateConfig']]],
  ['reg_5ffolder',['reg_folder',['../classelex_1_1libupdate_1_1_update_config.html#acd245f7aa7f9e99c4d3d32c0318a05b4',1,'elex::libupdate::UpdateConfig']]],
  ['retry',['Retry',['../classelex_1_1libupdate_1_1_update_info.html#a29124cd4489f1dd2c8476d5585e80193',1,'elex::libupdate::UpdateInfo']]],
  ['return_5fcontinue_5fupdate',['RETURN_CONTINUE_UPDATE',['../classelex_1_1libupdate_1_1_update_config.html#acd67359d717d2aec754a6e31ad957fb2ad63e0f739bf954c80e0dc6af6b0abb1c',1,'elex::libupdate::UpdateConfig']]],
  ['return_5fexit_5fapp',['RETURN_EXIT_APP',['../classelex_1_1libupdate_1_1_update_config.html#acd67359d717d2aec754a6e31ad957fb2a141d9c57aafa47e426ef47baede624ca',1,'elex::libupdate::UpdateConfig']]],
  ['return_5fexit_5fupdate',['RETURN_EXIT_UPDATE',['../classelex_1_1libupdate_1_1_update_config.html#acd67359d717d2aec754a6e31ad957fb2a48d76bcdbbc3d4fc9d3c9b9772be6b61',1,'elex::libupdate::UpdateConfig']]],
  ['return_5frun_5fupdate_5fand_5fexit_5fapp',['RETURN_RUN_UPDATE_AND_EXIT_APP',['../classelex_1_1libupdate_1_1_update_config.html#acd67359d717d2aec754a6e31ad957fb2a2ff126241dbdb3cf6d88b7cb76f73869',1,'elex::libupdate::UpdateConfig']]],
  ['run_5farguments',['run_arguments',['../classelex_1_1libupdate_1_1_update_config.html#a4a228bf6a992a0d1f44685cb3dbbe750',1,'elex::libupdate::UpdateConfig']]],
  ['runtime_5fretry',['runtime_retry',['../classelex_1_1libupdate_1_1_update_info.html#a7cfb046a2a9258c81608a3a0d79a1f54',1,'elex::libupdate::UpdateInfo']]]
];
