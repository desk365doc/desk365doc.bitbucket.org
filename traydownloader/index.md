﻿TrayDownloader使用说明
用法：
TrayDownloader [-url download-url] [-file saved-file-path] [[-exe run-args] | [-dll]] [-hide] [-pipe pipe-name] [-uid unique-id]
选项：
	-url	download-url	必须，指定需要下载的文件的url，如：-url "http://update.desk-365.com/files/Desk365_update_v1.4.12.4862.exe"
	-file	saved-file-path	必须，指定保存的文件路径，如：-file "C:\\test.txt"
	-exe	run-args		将下载的文件作为exe文件，使用管理员权限执行，run-args为执行参数。
							当参数有多个时需要放在一对"中。如：-exe "-c -b"
							当参数为空时，run-args为空字符串。如：-exe ""
	-dll					将下载的文件作为dll进行动态加载，并寻找执行RunMain函数。此项未测试。
	-hide					隐藏右下角托盘。显示托盘未测试。
	-pipe	pipe-name		指定下载信息输出管道。当此项指定时，下载过程中会将进度等信息（struct pipe_msg）往此管道写入。
							
	-uid	unique-id		指定id。
注意：下载的url的http response header中需包含"Content-Length"项方可下载成功。
/////////////////////////////////////////////////////////////////////////////	
#define P_ERROR	1
#define P_DOWNLOAD_START 2
#define P_DOWNLOAD_PROGRESS	3
#define P_DOWNLOAD_SUCCESS	4
#define P_DOWNLOAD_FAILED	5

#define MAX_LENGTH 400
struct pipe_msg{
	int uid;
	int op_code;
	size_t complete_size;
	size_t total_size;
	TCHAR info[MAX_LENGTH];
};